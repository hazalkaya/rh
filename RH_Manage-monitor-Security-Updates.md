## 1. Identifying security updates

- You can list all available security updates for your system by using the `yum` utility. 

*  List all available security updates which have not been installed on the host: 

```
# yum updateinfo list updates security
```
*  List all security updates which are installed on the host: 

```
# yum updateinfo list security --installed
```
* Display a specific advisory: 

```
# yum updateinfo info <Update ID>
```
## 2. Installing security updates

-  Install security updates using `yum` utility: 
```
# yum update --security
```
**Note !!**

The `--security` parameter is important. Without it, `yum update` installs all updates, including bug fixes and enhancements. 

- Optional: list processes that require a manual restart of the system after installing the updated packages: 
```
# yum needs-restarting
1107 : /usr/sbin/rsyslogd -n
1199 : -bash
```
Note !

This command lists only processes that require a restart, and not services. That is, you cannot restart processes listed using the systemctl utility. For example, the bash process in the output is terminated when the user that owns this process logs out.

### 2.2. Installing a security update provided by a specific advisory

 In certain situations, you might want to install only specific updates. For example, if a specific service can be updated without scheduling a downtime, you can install security updates for only this service, and install the remaining security updates later. 
 
 -  Install a specific advisory: 
 
 ```
 # yum update --advisory=<Update_ID>
 ```
 
 Important !

You can update to apply a specific advisory with a minimal version change by using the `yum upgrade-minimal <Update_ID>` command. 

### 2.3. Installing security updates automatically

-  Install dnf-automatic using yum 

```
# yum install dnf-automatic
```

-  Open the `/etc/dnf/automatic.conf` file in a text editor of your choice, for example: 
```
# vi /etc/dnf/automatic.conf
```

 Configure the `upgrade_type = security` option in the `[commands]` section: 
 
```
 [commands]
 #  What kind of upgrade to perform:
 # default                            = all available upgrades
 # security                           = only the security upgrades
 upgrade_type = security
```
 Enable the systemd timer unit 
 ```
 # systemctl enable --now dnf-automatic-install.timer
 ```
 
 REFERENCES
 ---------
 1. https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/8/html/managing_and_monitoring_security_updates/index
 
