Modify the `systemd` unit files and extend the default configuration, examine the system boot performance and optimize `systemd` to shorten the boot time.

## 1. Working with systemd unit files

The `systemd` unit files present your system resources. As a system administrator, you can perform the following advanced tasks:

* Create custom unit files
* Modify existing unit files
* Work with instantiated units 

#### 1.1. Introduction to unit files

A unit file contains configuration directives that describe the unit and define its behavior. Several `systemctl` commands work with unit files in the background. To make finer adjustments, you can edit or create unit files manually. You can find three main directories where unit files are stored on the system, the `/etc/systemd/system/` directory is reserved for unit files created or customized by the system administrator.

Unit file names take the following form: 

    <unit_name>.<type_extension>


Here, `unit_name` stands for the name of the unit and `type_extension` identifies the unit type. 

For example, you can find an `sshd.service` as well as an `sshd.socket` unit present on your system. 

#### 1.2. Systemd unit files locations

You can find the unit configuration files in one of the following directories: 

* `/usr/lib/systemd/system/` 
    
    `systemd` unit files distributed with installed RPM packages. 
* `/run/systemd/system/`
    
    `systemd` unit files created at run time. This directory takes precedence over the directory with installed service unit files. 
* `/etc/systemd/system/`
   
    `systemd` unit files created by using the systemctl enable command as well as unit files added for extending a service. This directory takes precedence over the directory with runtime unit files. 

The default configuration of `systemd` is defined during the compilation and you can find the configuration in the `/etc/systemd/system.conf` file. By editing this file, you can modify the default configuration by overriding values for systemd units globally. 

For example, to override the default value of the timeout limit, which is set to 90 seconds, use the `DefaultTimeoutStartSec` parameter to input the required value in seconds. 

    DefaultTimeoutStartSec=required value

### REFERENCES
----------
1. https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/8/html/using_systemd_unit_files_to_customize_and_optimize_your_system/index
