# Upgrading from RHEL 7 to RHEL 8

## KEY MIGRATION TERMINOLOGY

**Update**  
    In RHEL, an update relates to minor release, for example, updating from RHEL 8.1 to 8.2.

**Upgrade**  
    When you upgrade RHEL, you have two options:  
    - **In-place upgrade**: You replace the earlier version with the new version **without removing** the earlier version first. The installed applications and utilities, along with the configurations and preferences, are incorporated into the new version.  
    - **Clean install**: A clean install **removes** all traces of the previously installed operating system, system data, configurations, and applications and installs the latest version of the operating system. A clean install is ideal if you do not need any of the previous data or applications on your systems or if you are developing a new project that does not rely on prior builds.

**Operating System Conversion**  
    A conversion is when you convert your operating system from a different Linux distribution to RHEL.

**Migration**  
    Typically, a migration indicates a change of platform: software or hardware. Moving from Windows to Linux is a migration. Moving a user from one laptop to another or a company from one server to another is a migration. However, most migrations also involve upgrades, and sometimes the terms are used interchangeably.  
    - **Migration to RHEL**: Conversion of an existing operating system to RHEL.  
    - **Migration across RHEL**: Upgrade from one version of RHEL to another.  

**NOTE**    
    **LEAPP** is a CLI tool that helps users with the porting and installation process for Red Hat Enterprise Linux, making your in-place upgrade easier.
    An in-place upgrade is the recommended and supported way to upgrade your system to the next major version of RHEL.  
    Red Hat Content Delivery Network: CDN  
    Red Hat Simple Content Access: SCA


### REFERENCES
1. https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/8/pdf/upgrading_from_rhel_7_to_rhel_8/red_hat_enterprise_linux-8-upgrading_from_rhel_7_to_rhel_8-en-us.pdf
